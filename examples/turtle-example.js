var turnRandomness = 90;

window.onload = function() {
    // Get a reference to the canvas object
    var canvas = document.getElementById('canvas');
    // Create an empty project and a view for the canvas:
    paper.setup(canvas);

    var turtles = [];

    let turtle = new Turtle(
        paper.view.viewSize.width/2,
        1,
        {
            strokeColor: 'black', 
            strokeWidth: 4,
        }
    );
    turtle.turn(90);

    turtles.push(turtle);

    turtle = new Turtle(
        paper.view.viewSize.width/2,
        paper.view.viewSize.height-1,
        {
            strokeColor: 'black', 
            strokeWidth: 4,
        }
    );
    turtle.turn(-90);

    turtles.push(turtle);

    paper.view.onFrame = function(event) {
        if (event.count <= 2000) {
            turtles.forEach(function(turtle) {
                turtle.turn(getRandomInt(-turnRandomness,turnRandomness));
    
                var newPoint = turtle.forward(100);
    
                turtle.turn(getRandomInt(-30,30));
    
                if (newPoint.x > paper.view.viewSize.width || newPoint.x < 0) {
                    turtle.turn(90);
                }
                if (newPoint.y > paper.view.viewSize.height || newPoint.y < 0) {
                    turtle.turn(90);
                }
    
                if (event.count % 10 == 0) {
                    turtle.path.simplify(1);
                }
            });
        }
    }


    paper.view.draw();
}


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
  }
  
function getDistanceBetweenPoints(point1, point2) {
    return Math.hypot(point1.x-point2.x, point1.y-point2.y);
}