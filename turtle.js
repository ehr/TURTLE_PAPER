class Turtle {
    constructor(x, y, properties) {
        this.pathProps = properties;

        this.pathGroup = new paper.Group();
        this.path = this._initPath();

        this.pathGroup.addChild(this.path);

        this.currentPos = {x: x, y: y};
        this.path.add(new paper.Point(this.currentPos.x,this.currentPos.y))
        this.angle = 0;
        this.penIsDown = true;
    }
    _initPath() {
        let path = new paper.Path(this.pathProps);
        return path;
    }
    forward(amount) {
        let currentPos = this.currentPos;

        let x = currentPos.x + (amount * Math.cos(this._angleRadians()))
        let y = currentPos.y + (amount * Math.sin(this._angleRadians()))

        let newpoint = new paper.Point(x, y);
        
        if (this.penIsDown) {
            if (!this.path.lastSegment) {
                this.path.add(this.currentPos);
            }
            this.path.add(newpoint);
            this.currentPos = {x: x, y: y};
        }
        else {
            this.path = this._initPath(this.pathProps);
            this.pathGroup.addChild(this.path);
            this.currentPos = {x: x, y: y};
        }

        return this.currentPos;
    }
    turn(amount) {
        this.angle += amount;
        if (this.angle > 360 || this.angle < -360) {
            this.angle = 0+amount;
        }
    }
    penDown() {
        this.penIsDown = true;
    }
    penUp() {
        this.penIsDown = false;
    }
    _angleRadians() {
        return this.angle * Math.PI / 180;
    } 
}